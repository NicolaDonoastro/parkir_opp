// ============== Class Vehicle as Parent ================
// Vehicle can receive Ticket |  setTicket()
// Vehicle can set Vehicle    |  setVehicleLocation()

class Vehicle {
    constructor(name,type) {
        this.name = name;
        this.type = type;
        this.ticket = '';
        this.alreadyParking = false;

    }

    setTicket(ticket){
        this.ticket = ticket
    }

    setVehicleLocation(vehicleLocation) { 
        this.alreadyParking = vehicleLocation; 
    }

    getType(){
        console.log(`Vehicle type is ${this.type}`)
    }
}

class Car extends Vehicle {
    constructor(name, type) {
        super(name, type, ticket, alreadyParking);
        this.car = 0;

       
    }

    setCar(car) {
        this.car = car;
    }

    
}

class Motorcycle extends Vehicle {
    constructor(name, type ) {
        super(name, type, ticket, alreadyParking);
        this.motorcycle = 0;
    }

    setMotorcycle(motorcycle) {
        this.motorcycle = motorcycle;
    }

   
}

class ParkingArea {
    constructor(name, type) {
        super(name, type);
        this.carPark = 30;
        this.motorcyclePark = 50;
        this.vehicleLocation = 0;
        this.carSpaceAvailable = true; 
        this.motorSpaceAvailable = true;
    }

    greeting() {
        console.log(`Welcome ${this.name} ${this.type} in Parking Area`);
    }

    setCarPark(carPark) {
        this.carPark -= carPark;
    }



    // Check the car park availaibility
    isCarParkAvailable() {
        if (this.carPark > 0) {
            this.setCarSpaceAvailable(true); 
        } else { 
            this.setCarSpaceAvailable(false);
        }
    }

    setCarSpaceAvailable(available){ 
        this.carSpaceAvailable = available 
    } 
        
    setMotorcyclePark(motorcyclePark){
        this.motorcyclePark -= motorcyclePark
    }


    // Check the motorcycle park availaibility
    isMotorcycleParkAvailable() { 
        if (this.motorcyclePark > 0) {
            this.setMotorSpaceAvailable(true);
        } else { 
            this.setMotorSpaceAvailable(false); 
        }
    }

    setMotorSpaceAvailable(available){
        this.motorSpaceAvailable = available
    }

    
}

class ParkingMachine {
    constructor(name,type) {
        super(name,type)
        this.carCost = 5000;
        this.motorcycleCost = 2000;
        this.entranceTickets = []
    }

    // Generate ticket
    setEntranceTickets(ticketId) {
        this.entranceTickets.push(ticketId)
    }

    returnTheTicket(ticketId){
        
        let filteredTicket = this.entranceTickets.filter(
            (item) => !ticketId.includes(item)
        );

        this.entranceTickets = filteredTicket
    }

  

    vehicleIn() { 

        // Check if Car is going to park
        if (Car.type === "Car") { 

            // Check car park if it's available
            ParkingArea.isCarParkAvailable()

                // If car space is available car can get in
                if (ParkingArea.carSpaceAvailable) { 


                    if(Car.alreadyParking) {  
                        console.log('You already in') 
                    } else { 
                        const ticketId = Date.now().toString()
                        Car.setTicket(ticketId) 
                        ParkingMachine.setEntranceTickets(ticketId) 
                        ParkingArea.setCarPark(1)
                        Car.setVehicleLocation(true)
                        console.log('Welcome to Parking Area')
                    }
                }

                // If car park is not available, car can't get in
                else {
                    console.log("Car Space is not Available")
                }
        } 

        // Check if Motorcycle is going to park
        else if(Motorcycle.type === "Motorcycle") { // kalau mau pake metode ini, hati hati sama penggunaan huruf kapital co

            // Check Motorcycle park if it's available
            ParkingArea.isMotorcycleParkAvailable()

                // If Motorcycle space is available Motorcycle can get in
                if(Motorcycle.alreadyParking){ 

                    // Check if the
                    if(Motorcycle.alreadyParking){
                            console.log('You already in')  
                    } 
                    
                    else { 
                        const ticketId = Date.now().toString() 
                        Motorcycle.setTicket(ticketId) 
                        ParkingMachine.setEntranceTickets(ticketId) 
                        ParkingArea.setMotorcyclePark(1)
                        Motorcycle.setVehicleLocation(true)
                        console.log('Welcome to Parking Area')
                    }
                } 
                
                // If Motorcycle park is not available, Motorcycle can't get in
                else {
                    console.log("Motorcycle Space is not Available")
                }
        } 
        
        else {
            console.log('Your vehicle is not car nor Motorcycle!')
        }
    }

    vehicleOut() {
        // Check if Car is going to out park
        if (Car.type === "Car") {
            if(Car.alreadyParking === false) {  
                console.log('You already out parking area') 
            } else { 
               
                ticketId = Vehicle.ticket;
                ParkingMachine.returnTheTicket(ticketId) 
                Car.setTicket("") 
                ParkingArea.setCarPark(-1)
                Car.setVehicleLocation(False)
                console.log('Bye bye have a nice day ')
            }
        }  
        
        // Check if Motorcycle is going to park
        else if(Motorcycle.type === "Motorcycle") { 
            if(Motorcycle.alreadyParking === false) {  
                console.log('You already out parking area') 
            } else {    
                ticketId = Vehicle.ticket;
                ParkingMachine.returnTheTicket(ticketId) 
                Motorcycle.setTicket("") 
                ParkingArea.setCarPark(-1)
                Car.setVehicleLocation(False)
                console.log('Bye bye have a nice day ')
            }
        }
    }
}